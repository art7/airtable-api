var Airtable = require("airtable");

const fs = require("fs");
const latest = require("./latest.json");
const suppliersNewest = require("./suppliers-newest.json");
const throttledQueue = require("throttled-queue");

Airtable.configure({
  endpointUrl: "http://api.airtable.com",
  apiKey: "key8tmWWPOhbQh5aX"
});
const base = Airtable.base("appexrCSBIfV0XJHs");

let interface = {
  suppliers: "Suppliers",
  shipping: "Shipping",
  supplierLocations: "Supplier Locations",
  supplierContacts: "Supplier Contacts",
  commodity: "Commodity",
  shipping: "Shipping",
  shippingContact: "Shipping Contact"
};

/**
 *
 * @param {String} table - table that we want get data from
 * returns 100 recods per page
 */
function callAirtableApi(table) {
  let records1 = [];
  return new Promise((resolve, reject) => {
    base(table)
      .select()
      .eachPage(
        (records, fetchNext) => {
          records1 = records1.concat(
            records.map(record => {
              return {
                id: record.getId(),
                name: record.get("Name"),
                "address 1": record.get("Address 1"),
                "address 2": record.get("Address 2"),
                city: record.get("City"),
                state: record.get("State"),
                zip: record.get("Zip"),
                locations: record.get("Locations")
              };
            })
          );
          fetchNext();
        },
        err => {
          if (err) {
            reject(err);
          } else {
            resolve(records1);
          }
        }
      );
  });
}

let throttle = throttledQueue(4, 1000);

/**
 *
 * @param {Object} record - record that we need to look up
 * @param {String} table - table where we look for the record
 */
const getLocationById = (record, table) => {
  return new Promise((resolve, reject) => {
    const retrievedRecord = (err, foundRecord) => {
      if (err) {
        reject(err);
        return;
      }
      foundRecord = { id: foundRecord.id, ...foundRecord.fields };
      // console.log("Pulling info for the record ", foundRecord.id);
      resolve(foundRecord);
    };
    throttle(() => {
      // console.log('Throttled');
      base(table).find(record, retrievedRecord);
    });
  });
};

// callAirtableApi(interface.suppliers).then(data => getLocations(data));
// .then(value => {
//   fs.writeFile("getLocations1.json", JSON.stringify(value), err => {});
// });
// .then(v => {
//   console.log(v);
// });

function getLocations(data) {
  return new Promise((resolve, reject) => {
    data.forEach(async supplier => {
      if (
        supplier.locations != null &&
        typeof supplier.locations[0] == String
      ) {
        supplier.locations.map(location => {
          console.log("Match!");

          return getLocationById(location, interface.supplierLocations);
        });

        Promise.all(locations)
          .then(values => {
            console.log(values);
            resolve(values);
          })
          .catch(e => reject(e));
      }
    });

    // setTimeout(() => {
    //   console.log(data);
    //   fs.writeFile("current.json", JSON.stringify(data), err => {});
    // }, 20000);
  });
}

function getLocations1(data) {
  return new Promise((resolve, reject) => {
    const requests = data.reduce((prev, cur) => {
      if (cur.locations) {
        prev[cur.id] = cur.locations.map(location => {
          return getLocationById(location, interface.supplierLocations).catch(
            e => Promise.resolve(e)
          );
        });
      }
      return prev;
    }, {});
    const requests1 = [];
    for (r in requests) {
      requests1.push(
        Promise.all(requests[r]).then(v => {
          requests[r] = v;
          return v;
        })
      );
    }
    Promise.all(requests1).then(() => {
      resolve(requests);
    });
  });
}

const getLocations2 = async data => {
  return new Promise((resolve, reject) => {
    data.map(supplier => {
      if (supplier.locations != undefined) {
        return supplier.locations.map(async (location, index) => {
          console.log("Index", index);
          resolve(supplier);
        });
      }
    });
  });
};


//getting specific record

(async () => {
  try {
    let retrieved = await getLocationById(
      "recJVdC5E61LEgjyd",
      interface.supplierLocations
    );
    setTimeout(() => {
      fs.writeFile(
        "recnOodm13EUkYgWi.json",
        JSON.stringify(retrieved),
        err => {}
      );
    }, 10000);
  } catch (err) {
    console.log(err);
  }
})();

